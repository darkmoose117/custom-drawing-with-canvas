package com.darkmoose117.canvas.viewgroup;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.darkmoose117.canvas.R;

public class PlaidViewGroup extends LinearLayout {

    // Paints
    private Paint mVerticalPaint;
    private Paint mHorizontalPaint;

    // Dimens
    private float mStroke;
    private float mSpacing;

    public PlaidViewGroup(Context context) {
        this(context, null);
    }

    public PlaidViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlaidViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        Resources res = getResources();
        mStroke = res.getDimensionPixelSize(R.dimen.view_group_stroke_width);
        mSpacing = res.getDimensionPixelOffset(R.dimen.view_group_spacing);

        mVerticalPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mVerticalPaint.setStyle(Paint.Style.STROKE);
        mVerticalPaint.setColor(ContextCompat.getColor(context, R.color.vertical_plaid_color));
        mVerticalPaint.setStrokeWidth(mStroke);

        mHorizontalPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mHorizontalPaint.setStyle(Paint.Style.STROKE);
        mHorizontalPaint.setColor(ContextCompat.getColor(context, R.color.horizontal_plaid_color));
        mHorizontalPaint.setStrokeWidth(mStroke);

        // this must be called, or draw methods will never be called
        // Will Not Draw = false means we WILL draw.
        setWillNotDraw(false);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // this is called first. This is the same as any other View getting onDraw called.
        canvas.drawLine(0, 0, 0, getHeight(), mVerticalPaint);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {

        // This is drawn just before this view group has drawn it's children.
        float x = mSpacing + mStroke;
        canvas.drawLine(x, 0, x, getHeight(), mVerticalPaint);

        // All of these draw calls are happening after the child views have been laid out. So you
        // can iterate through them and draw based on their location. Doing this before super means
        // we'll draw these lines UNDER the children.
        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            canvas.drawLine(0, child.getBottom(), getWidth(), child.getBottom(), mHorizontalPaint);
        }

        // Calling super here will let whatever ViewGroup you're extending do it's job and draw all
        // the children. Note that it's not enforced that you call super here, but if you don't
        // you'll need to iterate through all your children and make them draw on this canvas.
        super.dispatchDraw(canvas);

        // Drawing based on the the children now will draw on top of them, since we've called
        // super.dispatchDraw(canvas)
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            canvas.drawLine(0, child.getTop(), getWidth(), child.getTop(), mHorizontalPaint);
        }

        // Here, you can draw on top of children that have just been drawn, however onDrawForeground
        // is usually more appropriate. (see below)
        x += mSpacing + mStroke;
        canvas.drawLine(x, 0, x, getHeight(), mVerticalPaint);
    }

    @Override
    public void onDrawForeground(Canvas canvas) {
        // This method is responsible for drawing all foreground content. This can often be
        // something like a Ripple Drawable for a pressed state, or perhaps a scroll bar.
        // Drawing BEFORE calling super will let us draw underneath default foreground stuff
        float x = getWidth();
        canvas.drawLine(x, 0, x, getHeight(), mVerticalPaint);

        // Calling super will run View's default foregound drawing, like set foreground drawables,
        // scroll bars, and over scroll fades.
        super.onDrawForeground(canvas);

        // After calling super, you're drawing on top of everything else previously drawn.
        x -= (mSpacing + mStroke);
        canvas.drawLine(x, 0, x, getHeight(), mVerticalPaint);
    }
}
