package com.darkmoose117.canvas.viewgroup

import android.os.Bundle

import com.darkmoose117.canvas.DemoActivity
import com.darkmoose117.canvas.R

class ViewGroupActivity : DemoActivity() {

    override val titleResId: Int = R.string.view_group_demo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_group)
    }
}
