package com.darkmoose117.canvas

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View

import com.darkmoose117.canvas.barchart.BarChartActivity
import com.darkmoose117.canvas.email.EmailActivity
import com.darkmoose117.canvas.touch.TouchTrackingActivity
import com.darkmoose117.canvas.viewgroup.ViewGroupActivity

import kotlinx.android.synthetic.main.activity_demo_launcher.*

class DemoLauncherActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo_launcher)

        bar_chart_demo.setOnClickListener(DemoClickListener(BarChartActivity::class.java))
        email_demo.setOnClickListener(DemoClickListener(EmailActivity::class.java))
        view_group_demo.setOnClickListener(DemoClickListener(ViewGroupActivity::class.java))
        touch_tracking_demo.setOnClickListener(DemoClickListener(TouchTrackingActivity::class.java))
    }

    private class DemoClickListener(private val activityClass: Class<*>) : View.OnClickListener {

        override fun onClick(v: View) {
            v.context.let {
                val intent = Intent(it, activityClass)
                it.startActivity(intent)
            }
        }

    }
}
