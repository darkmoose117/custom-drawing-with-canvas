package com.darkmoose117.canvas.touch

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PointF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator

import com.darkmoose117.canvas.R
import com.darkmoose117.canvas.extensions.getColorCompat
import kotlin.math.roundToInt

class TouchTrackingView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0)
: View(context, attrs, defStyleAttr) {

    // Drawing Objects
    private val paint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        color = context.getColorCompat(R.color.bar_color)
    }
    private val heartSize: Float = resources.getDimensionPixelSize(R.dimen.touch_path_bounds_size).toFloat()
    private val heartPath: Path = HeartPathHelper.getHeartOfSize(heartSize.roundToInt())

    // Rotation Animation
    private val rotationAnimator: ValueAnimator = ValueAnimator().apply {
        duration = 250
        setIntValues(-30, 0, 30)
        repeatCount = ValueAnimator.INFINITE
        repeatMode = ValueAnimator.REVERSE
        interpolator = AccelerateDecelerateInterpolator()
        addUpdateListener { invalidate() }
        start()
        pause()
    }

    // Touch
    private var isFingerDown = false
    private var finger: PointF = PointF()

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return when (event.action) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_MOVE -> {
                // If a finger is down and/or moving, update the location
                isFingerDown = true
                finger.set(event.x, event.y)

                // If our Animator isn't running, start it.
                if (rotationAnimator.isPaused) {
                    rotationAnimator.resume()
                }

                // Must call invalidate here
                invalidate()

                true
            }
            MotionEvent.ACTION_UP,
            MotionEvent.ACTION_CANCEL -> {
                if (isFingerDown) {
                    isFingerDown = false
                    rotationAnimator.pause()
                    invalidate()
                }
                super.onTouchEvent(event)
            }
            else -> super.onTouchEvent(event)
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        // If user isn't touching the screen, do nothing.
        if (!isFingerDown) {
            return
        }

        canvas.rotateAndTranslate(
                rotation = (rotationAnimator.animatedValue as Int).toFloat(),
                pivotX = finger.x,
                pivotY = finger.y,
                translationX = finger.x - heartSize / 2f,
                translationY = finger.y - heartSize / 2f) {
            drawPath(heartPath, paint)
        }
    }

    private inline fun Canvas.rotateAndTranslate(rotation: Float = 0f,
                                                 pivotX: Float = 0f,
                                                 pivotY: Float = 0f,
                                                 translationX: Float = 0f,
                                                 translationY: Float = 0f,
                                                 draw: Canvas.() -> Unit) {
        val checkpoint = save()
        rotate(rotation, pivotX, pivotY)
        translate(translationX, translationY)
        try {
            draw()
        } finally {
            restoreToCount(checkpoint)
        }
    }
}
