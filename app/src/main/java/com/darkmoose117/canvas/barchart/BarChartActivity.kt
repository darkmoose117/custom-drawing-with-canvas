package com.darkmoose117.canvas.barchart

import android.os.Bundle

import com.darkmoose117.canvas.DemoActivity
import com.darkmoose117.canvas.R

import kotlinx.android.synthetic.main.activity_bar_chart.*

class BarChartActivity : DemoActivity() {

    override val titleResId: Int = R.string.bar_chart_demo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bar_chart)

        animate_button.setOnClickListener { bar_chart.animateNewData() }
    }
}
