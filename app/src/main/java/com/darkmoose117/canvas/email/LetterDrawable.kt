package com.darkmoose117.canvas.email

import android.content.Context
import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.support.annotation.IntRange
import android.text.TextPaint

import com.darkmoose117.canvas.R
import com.darkmoose117.canvas.extensions.getColorCompat

class LetterDrawable(context: Context, char: Char) : Drawable() {

    private val letter = char.toString()
    private val myBounds: Rect = Rect()

    private val circlePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = context.getColorCompat(R.color.icon_color)
    }

    private val letterPaint: Paint = TextPaint(Paint.ANTI_ALIAS_FLAG).apply {
        color = context.getColorCompat(android.R.color.white)
        textSize = context.resources.getDimensionPixelSize(R.dimen.email_icon_text_size).toFloat()
        textAlign = Paint.Align.CENTER
    }

    private val letterTop = letterPaint.run {
        // Text draws from the baseline. We'll need to add some top padding to center the char
        // vertically.
        val textMathRect = Rect()
        letterPaint.getTextBounds(letter, 0, 1, textMathRect)
        textMathRect.height() / 2f
    }

    override fun onBoundsChange(bounds: Rect) {
        super.onBoundsChange(bounds)
        // Force matching width and height. Keep top/left and right the same, force bottom to fit.
        myBounds.set(bounds.left, bounds.top, bounds.right, bounds.top + bounds.width())
    }

    override fun draw(canvas: Canvas) {
        canvas.drawCircle(myBounds.exactCenterX(), myBounds.exactCenterY(),
                myBounds.width() / 2f, circlePaint)
        canvas.drawText(letter, myBounds.exactCenterX(), letterTop + myBounds.exactCenterY(),
                letterPaint)
    }

    override fun setAlpha(@IntRange(from = 0, to = 255) alpha: Int) {
        circlePaint.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        circlePaint.colorFilter = colorFilter
    }

    override fun getOpacity(): Int {
        return PixelFormat.OPAQUE
    }

}
